import random

def menu():
    # Get player numbers
    player_num = get_players_number()
    #Get lottery numbers
    lottery_nums = create_lottery_numbers()
    #Get the winning numbers
    winning_num = player_num.intersection(lottery_nums)
    print("You have just won yourself ${} by matching {}".format(len(winning_num)*1000, winning_num))

#  User can pick 6 numbers
def get_players_number():
    player_num_csv = input('Please enter your 6 comma-separeted numbers: ')
    # Create a set of integers from this number_csv 
    player_num_set = player_num_csv.split(',')
    player_num_set_int = {int(num) for num in player_num_set}
    return player_num_set_int

#Lottery calculates 6 random numbers (between 1 and 20)
def create_lottery_numbers():
    lottery_num = set()
    while len(lottery_num) < 6:
        rand_num = lottery_num.add(random.randint(0, 20))    
    return lottery_num 

menu()
