import random

magic_num = [random.randint(0, 9)]

def ask_user_and_check():
    user_pick =int(input("Please choose a number: ")) 
    if user_pick not in magic_num:
        return 'You missed it sir!'
    if user_pick in magic_num:
        return "You got it sir"

def run_program_x_times(x):
    for attempt in range(x):
        print("This your {} attempt".format(attempt))
        message = ask_user_and_check()
        print(message)

run_program_x_times(int(input('How many times do you wanna try:')))